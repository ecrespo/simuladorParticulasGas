# Simulador de Particulas de Gas Ideal.

## Descripción de la aplicación.

Se tiene una aplicación que el usuario le da clic en el menú, se abre una ventana, dicha ventana tiene una barra de menú donde se tiene las opciones de guardar o cargar los datos iniciales de la aplicación, se tiene un marco donde están los datos de entrada ( volumen, gas inerte, presión inicial y temperatura inicial). a la izquierda de este marco se tiene otro marco donde se visualiza un contenedor, donde hay 2 barras de desplazamiento vertical que permiten aumentar o disminuir la presión y la temperatura en el contenedor, así como un reloj que muestra el tiempo que transcurre al iniciar la simulación por medio de un boton de inicio, también se tiene un botón de pausa, uno de parada y otro de reset.  En el contenedor se muestra las particulas donde según el volumen, la variación de presión y temperatura, las particulas chocarán una a las otras según estas variaciones que el usuario las introduce por medio de las barras de desplazamiento de la presión y temperatura, o de los cambios en los valores iniciales ya mencionados, también se tiene la posibilidad de definir un tiempo final de ejecución de la simulación, que este dato lo introduce el usuario. La información de los cambios de presión se muestrán en un cuadro en el marco donde se realiza la simulación.


## Lista de objetos

La lista de objetos existentes en la aplicación son:

1. El usuario
2. La aplicación
3. La ventana.
4. La barra de botones.
5. Las opciones (guardar, cargar).
6. El marco (datos de entrada)
7. Los datos de entrada (presión, temperatura, volumén).
8. Otro marco (marco de simulación).
9. El contenedor del gas.
10. El gas ideal.
11. La barra de desplazamiento de la temperatura.
12. La barra de desplazamiento del volumen.
13. Botón de inicio/parada.
14. Botón de pausa.
15. Botón de reset (volver a condiciones iniciales).
16. La particula de gas.
17. Las particulas de gas.
18. Un reloj (cronometro).
19. El cuadro informativo
20. La presión actual (salida).

## Jerarquía de los objetos
A continuación se lista los objetos con su relación jerarquíca:

1. Aplicación.

1.1 Ventana.

1.1.1 grid.

1.1.1.1 Barra de botones.

1.1.1.1.1 Botón guardar
1.1.1.1.2 Botón cargar
1.1.1.1.3 Botón de Inicio/Parada.
1.1.1.1.4 Botón de pausa.
1.1.1.1.5 Botón de Reset.
1.1.1.2 Marco de datos de entrada.
1.1.1.2.1 Entrada de datos de Presión.
1.1.1.2.2 Entrada de datos de Temperatura.
1.1.1.2.3 Entrada de datos de Volumén.
1.1.1.2.4 Barra de desplazamiento de temperatura.
1.1.1.2.5 Barra de desplazamiento del volumen.
1.1.1.3 Marco de simulación.
1.1.1.3.1 Grid
1.1.1.3.1.1 Contenedor del gas ideal.
1.1.1.3.1.1.1 Gas Ideal.
1.1.1.3.1.1.1.1 Particula de gas Ideal.
1.1.1.3.1.1.1.2 Particulas de gas Ideal.
1.1.1.3.1.2 Reloj (cronometro).
1.1.1.3.1.3 Campo de la presión actual.

## Distribución de los Objetos.

En la siguiente figura se muestra la ventana de la aplicación.


## Referencias

A continuación se lista unos enlaces sobre referencias a la física de la dinámica de particulas:

1. http://dyndelion.com/index.php/proyectos/dinamica-de-particulas/
2. http://www3.gobiernodecanarias.org/medusa/lentiscal/1-CDQuimica-TIC/index.htm
3. http://www.sc.ehu.es/sbweb/fisica/estadistica/gasIdeal/gasIdeal.html
